var express = require('express');
var conference = require('../models/conference');
var hotel = require('../models/hotel');

module.exports = function (passport) {
    var router = express.Router();
   
   router.get('/hotel/add_hotel', function(req, res, next) {
        res.render('hotel/add_hotel');   
    });
    
    router.post('/add_hotel', function(req, res, next) {
    var nowy={
        nazwa:req.body.nazwa,
        adres:req.body.adres,
        email:req.body.email
    }
    
    hotel.create(nowy, function (id){
            console.log(nowy);
           res.render('hotel/add_hotel', {message: "Dodano hotel o id: " + id});  
            });
     });  
    router.get('/hotel/list_hotel', function (req, res) {
        hotel.showHotels(function (hotel) {
            res.render('hotel/list_hotel',{hotel: hotel});
        });
    });
     
    router.get('/edit_hotel/:id', function (req, res) {
        hotel.selectRowByField({id_hotel: req.params.id}, function (err, hotel) {
            console.log(hotel);
                res.render('hotel/edit_hotel', {edited_hotel: hotel});
        });
    });
    
    router.post('/edit_hotel/:id', function(req, res, next) {
        var update={
        nazwa:req.body.nazwa,
        adres:req.body.adres,
        email:req.body.email
    }
    hotel.updateRowByField({id_hotel: req.params.id}, update, function () {
            res.redirect('/hotel/list_hotel');
       });
       });
     return router;
};