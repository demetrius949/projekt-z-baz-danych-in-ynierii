/**
 * Created by Mateusz on 2015-05-25.
 */
var express = require('express');
var charges = require('../models/charges');
var conference = require('../models/conference');
var auth = require('../config/auth');
var jade = require('jade');
var fs = require('fs');
var pdf = require('html-pdf');

//komunikaty
var op_con=0;
var err_Nan=0;
var del_c=0;
var pc=0;

module.exports = function (passport) {
    var router = express.Router();

    
    router.get('/charges/generate_payment/:id/:idu', auth.isAdmin, function(req, res, next) {
        
        charges.selectRowByField({id_konferencja: req.params.id}, function(err, confna){
            charges.showProducts({id_konferencja: req.params.id}, function (conf) {
                charges.selectRowByFieldU({id_uzytkownika: req.params.idu}, function (err, confu) {
                    charges.selectSumCost({id_konferencja: req.params.id}, function (err, sconf) {
                        var fn = jade.compileFile('./views/charges/generate_payment.jade');
                        var html = fn({conference_name:confna, conference:conf, conference_users:confu, show_conferences:sconf});
                        pdf.create(html).toStream(function(err, stream){
                        stream.pipe(res);
                    });   
                });
            });
        });
        });
    });
    
    router.get('/charges/delete_cost', auth.isAdmin, function(req, res, next) {
        res.render('charges/delete_cost');
    });
    
    router.get('/charges/choose_conferenceu', auth.isAdmin, function(req, res, next) {
           
           if(pc)
           {
                charges.ShowConferences2(function(confs) {
                res.render('charges/choose_conferenceu', { conference: confs, message: "Oplata zostala potwierdzona" });
            });
                pc=0;
           }
            else
            {
                
            charges.ShowConferences2(function(confs) {
                res.render('charges/choose_conferenceu', { conference: confs });
            });
            }
    });
    
    router.get('/charges/payment_confirm', auth.isAdmin, function(req, res, next) {
        res.render('charges/payment_confirm');
    });

    router.post('/charges/payment_confirm', function(req, res) {
        console.log(req.body);
    });
    
    router.get('/charges/choose_conference', auth.isAdmin, function(req, res, next) {
        charges.ShowConferences2(function(confs) {
            if(err_Nan)
            {
                res.render('charges/choose_conference', { conference: confs, message: 'Podano zla kwote'});
                err_Nan=0;
            }
            else if(op_con){ 
                res.render('charges/choose_conference', { conference: confs, message: 'Dodano oplate'});
                op_con=0;
            }
            else if(del_c){ 
                res.render('charges/choose_conference', { conference: confs, message: 'Usunieto oplate'});
                del_c=0;
            }
            else res.render('charges/choose_conference', { conference: confs})
            });
        });

    
    
    
    router.get('/charges/review_costs/:id', auth.isAdmin, function(req, res, next) {
            //res.render('charges/review_costs', {/* users: usr*/ });
        charges.selectRowByField({id_konferencja: req.params.id}, function(err, confna){
            charges.showProducts({id_konferencja: req.params.id}, function (conf) {
                
                
                res.render('charges/review_costs', {conference: conf, conference_name:confna});
              
            });
        });

    });
    
    router.get('/charges/payment_list/:id',  auth.isAdmin, function(req, res, next) {
            //res.render('charges/review_costs', {/* users: usr*/ });
        charges.selectRowByField({id_konferencja: req.params.id}, function(err, confna){ 
            charges.GetUsers({id_konferencja: req.params.id}, function (conf) {
                
                
                res.render('charges/payment_list', {conference: conf, conference_name: confna});
              
            });
        });
        
    });
    
    
    router.get('/charges/delete_cost/:id',  auth.isAdmin, function (req, res, next) {
        console.log(req.params.id);
        charges.selectRowByFieldO({id_oplaty: req.params.id},function (err, charg) {
            res.render('charges/delete_cost', {charge: charg});
        });
    });
    
    
    router.post('/charges/delete_cost/:id', function (req, res) {
        
        console.log(req.params.id);
         charges.deleteCost({id_oplaty: req.params.id}, function () {
            del_c=1;
            res.redirect('/charges/choose_conference');
            
            //res.render('charges/choose_conference',{ message: "test"});
            
        });
        
        
        
        
    });
    
    router.get('/payment_confirm/:id',  auth.isAdmin, function (req, res, next) {
        console.log(req.params.id);
        charges.selectRowByFieldP({id_przypisanie: req.params.id},function (err, assi) {
        //console.log(assi, next);
            res.render('charges/payment_confirm', {assign: assi});
        });
    });
    
    
    router.post('/payment_confirm/:id', function (req, res) {
        console.log(req.params.id);
        {
        
        var obj = {
            wplata: 1
        };
        console.log(req.params.id);
        charges.updateRowByFieldP({id_przypisanie: req.params.id}, obj, function () {
            pc=1;
            res.redirect('/charges/choose_conferenceu');
            
        });
        }
        
        
        
        
    });
    
    router.post('/charges/review_costs/:id', function (req, res, next) {
        console.log(req.params.id);
        
        
        
        var obj = {
            kwota: req.body.kwota,
            nazwa: req.body.nazwa,
            konferencja_id_konferencja: req.params.id,
            
            
        };
        console.log(req.body.kwota);
        console.log( req.body.nazwa);
        console.log(req.params.id);
        if(isNaN(obj.kwota)||obj.kwota<=0)
        {
            err_Nan=1;
            res.redirect('/charges/choose_conference');
        }
        else{
        charges.createCost(obj, function () {
            console.log(obj);
            op_con=1;
            res.redirect('/charges/choose_conference');
        
        });
        }
        
        
        
        
    });
    
    router.get('/set_cost/:id', auth.isAdmin, function (req, res) {
        charges.selectRowByField({id_konferencja: req.params.id}, function (err, conf) {
            
           res.render('charges/set_cost', {conference: conf});
        });
    });
    
    router.post('/set_cost/:id', function (req, res) {
        
        console.log(req.params.id);
        
        {
        
        var obj = {
            koszty: req.body.koszty
        };
        conference.updateRowByField({id_konferencja: req.params.id}, obj, function () {
            res.redirect('/charges/choose_conference');
            
        });
        }
    });

    
    return router;
};

